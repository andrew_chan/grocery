Grocery::Application.routes.draw do
  match '/rate' => 'rater#create', :as => 'rate', via: [:get, :post]
  
  resources :products do
    collection do
      get 'search'
    end
  end

  resources :users
    
  resources :ratings, only: :update
  
  root 'products#index'

  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
  match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]
  match 'products/:id/like', to: 'products#like', via: [:get, :post], :as => :like_product
  match 'products/:id/dislike', to: 'products#dislike', via: [:get, :post], :as => :dislike_product
  

end
