class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.text :item_id
      t.text :provider_id
      t.text :ref_id
      t.text :brand_id
      t.text :source_url
      t.text :item_name
      t.text :item_description
      t.date :post_date
      t.datetime :crawl_time
      t.binary :target_audience
      t.binary :season
      t.text :currency_code
      t.decimal :price
      t.decimal :tax_and_levy
      t.decimal :discount
      t.decimal :delivery_charges
      t.text :src_id
      t.binary :src_popular
      t.binary :in_stock
      t.text :temp_img_urls
      t.integer :likes_count

      t.timestamps
    end
  end
end
