# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130917085917) do

  create_table "likes", force: true do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "likes", ["product_id"], name: "index_likes_on_product_id", using: :btree
  add_index "likes", ["user_id"], name: "index_likes_on_user_id", using: :btree

  create_table "products", force: true do |t|
    t.text     "item_id"
    t.text     "provider_id"
    t.text     "ref_id"
    t.text     "brand_id"
    t.text     "source_url"
    t.text     "item_name"
    t.text     "item_description"
    t.date     "post_date"
    t.datetime "crawl_time"
    t.binary   "target_audience"
    t.binary   "season"
    t.text     "currency_code"
    t.decimal  "price",            precision: 10, scale: 0
    t.decimal  "tax_and_levy",     precision: 10, scale: 0
    t.decimal  "discount",         precision: 10, scale: 0
    t.decimal  "delivery_charges", precision: 10, scale: 0
    t.text     "src_id"
    t.binary   "src_popular"
    t.binary   "in_stock"
    t.text     "temp_img_urls"
    t.integer  "likes_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ratings", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.integer  "score",      default: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ratings", ["product_id"], name: "index_ratings_on_product_id", using: :btree
  add_index "ratings", ["user_id"], name: "index_ratings_on_user_id", using: :btree

  create_table "ss_additional_attr", primary_key: "aa_id", force: true do |t|
    t.string "grp_identity_id", limit: 14,  null: false
    t.string "fav_brands",      limit: 300
  end

  add_index "ss_additional_attr", ["grp_identity_id"], name: "grp_identity_id", using: :btree

  create_table "ss_also_seen_on", id: false, force: true do |t|
    t.integer "repin_id",     null: false
    t.integer "src_repin_id", null: false
  end

  add_index "ss_also_seen_on", ["src_repin_id"], name: "src_repin_id", using: :btree

  create_table "ss_audit_trail", primary_key: "trail_id", force: true do |t|
    t.string    "ind_identity_id", limit: 14,  null: false
    t.timestamp "login_time",                  null: false
    t.binary    "ip_address",      limit: 32
    t.string    "user_agent",      limit: 100
    t.string    "geo_code",        limit: 4
    t.timestamp "logout_time"
  end

  add_index "ss_audit_trail", ["ind_identity_id"], name: "ind_identity_id", using: :btree

  create_table "ss_audit_trail_extended", id: false, force: true do |t|
    t.integer   "trail_id",    limit: 8, null: false
    t.timestamp "action_time",           null: false
    t.string    "action_code", limit: 2, null: false
  end

  create_table "ss_availability", id: false, force: true do |t|
    t.string "item_id",        limit: 33, null: false
    t.string "color_code",     limit: 3,  null: false
    t.string "size_code_type", limit: 3,  null: false
    t.string "size_code",      limit: 6,  null: false
    t.binary "availability",   limit: 1,  null: false
  end

  add_index "ss_availability", ["item_id", "size_code_type", "size_code"], name: "item_id", using: :btree

  create_table "ss_brand", primary_key: "brand_id", force: true do |t|
    t.string "brand_name", limit: 40, null: false
    t.string "style_loc",  limit: 2
  end

  create_table "ss_color", id: false, force: true do |t|
    t.string "item_id",    limit: 33, null: false
    t.string "color_code", limit: 3,  null: false
    t.binary "avail",      limit: 1,  null: false
  end

  create_table "ss_delivery_charges", id: false, force: true do |t|
    t.integer "provider_id",                                      null: false
    t.string  "area_code",      limit: 3,                         null: false
    t.integer "num_days_min",                                     null: false
    t.integer "num_days_max",                                     null: false
    t.decimal "flat_min",                 precision: 6, scale: 2, null: false
    t.decimal "flat_max",                 precision: 6, scale: 2, null: false
    t.date    "effective_date",                                   null: false
  end

  create_table "ss_external_network_vendor", primary_key: "id_prefix", force: true do |t|
    t.string "network_name", limit: 20,  null: false
    t.string "url",          limit: 200, null: false
  end

  create_table "ss_identity", primary_key: "identity_id", force: true do |t|
    t.string "external_id",    limit: 20
    t.string "username",       limit: 20
    t.string "first_name",     limit: 20,  null: false
    t.string "middle_name",    limit: 20
    t.string "last_name",      limit: 20,  null: false
    t.string "nickname",       limit: 30
    t.string "email",          limit: 50,  null: false
    t.date   "birth_date"
    t.binary "gender",         limit: 2
    t.string "introduction",   limit: 500
    t.string "interests",      limit: 200
    t.string "occupation",     limit: 30
    t.binary "marital_status", limit: 2
    t.string "location",       limit: 30
  end

  create_table "ss_identity_group", id: false, force: true do |t|
    t.string "grp_identity_id",  limit: 14, null: false
    t.string "ind_identity_id",  limit: 14, null: false
    t.binary "primary_identity", limit: 1,  null: false
    t.binary "special",          limit: 2
  end

  add_index "ss_identity_group", ["ind_identity_id"], name: "ind_identity_id", using: :btree

  create_table "ss_item", primary_key: "item_id", force: true do |t|
    t.integer   "provider_id",                                           null: false
    t.string    "ref_id",           limit: 20
    t.integer   "brand_id",                                              null: false
    t.string    "source_url",       limit: 512,                          null: false
    t.string    "item_name",        limit: 100,                          null: false
    t.string    "item_description", limit: 1024
    t.date      "post_date"
    t.timestamp "crawl_time",                                            null: false
    t.binary    "target_audience",  limit: 2,                            null: false
    t.binary    "season",           limit: 2
    t.string    "currency_code",    limit: 3,                            null: false
    t.decimal   "price",                         precision: 9, scale: 2, null: false
    t.decimal   "tax_and_levy",                  precision: 9, scale: 2
    t.decimal   "discount",                      precision: 9, scale: 2
    t.decimal   "delivery_charges",              precision: 6, scale: 2
    t.string    "src_id",           limit: 20,                           null: false
    t.binary    "src_popular",      limit: 1
    t.binary    "in_stock",         limit: 1
    t.string    "temp_img_urls",    limit: 1000,                         null: false
  end

  add_index "ss_item", ["brand_id"], name: "brand_id", using: :btree
  add_index "ss_item", ["provider_id"], name: "provider_id", using: :btree

  create_table "ss_item_category", id: false, force: true do |t|
    t.string  "item_id",      limit: 33, null: false
    t.integer "category_id",             null: false
    t.integer "subcat_id"
    t.integer "subsubcat_id"
  end

  create_table "ss_item_grp", id: false, force: true do |t|
    t.integer   "item_grp_id",                null: false
    t.string    "item_id",         limit: 33, null: false
    t.string    "ind_identity_id", limit: 14
    t.timestamp "grp_time"
  end

  add_index "ss_item_grp", ["ind_identity_id"], name: "ind_identity_id", using: :btree
  add_index "ss_item_grp", ["item_id"], name: "item_id", using: :btree

  create_table "ss_item_tag", id: false, force: true do |t|
    t.string  "item_id",    limit: 33, null: false
    t.integer "tag_id",                null: false
    t.binary  "tag_status", limit: 1,  null: false
  end

  add_index "ss_item_tag", ["tag_id"], name: "tag_id", using: :btree

  create_table "ss_like", id: false, force: true do |t|
    t.string    "ind_identity_id", limit: 14, null: false
    t.integer   "repin_id",                   null: false
    t.timestamp "like_time",                  null: false
  end

  add_index "ss_like", ["repin_id"], name: "repin_id", using: :btree

  create_table "ss_msg_board", primary_key: "msg_id", force: true do |t|
    t.integer   "repin_id",                     null: false
    t.string    "ind_identity_id", limit: 14,   null: false
    t.string    "msg",             limit: 1024, null: false
    t.timestamp "msg_time",                     null: false
  end

  add_index "ss_msg_board", ["ind_identity_id"], name: "ind_identity_id", using: :btree
  add_index "ss_msg_board", ["repin_id"], name: "repin_id", using: :btree

  create_table "ss_networks", id: false, force: true do |t|
    t.string  "grp_identity_id1", limit: 14, null: false
    t.string  "grp_identity_id2", limit: 14, null: false
    t.integer "link_count",       limit: 1,  null: false
  end

  add_index "ss_networks", ["grp_identity_id2"], name: "grp_identity_id2", using: :btree

  create_table "ss_provider", primary_key: "provider_id", force: true do |t|
    t.string  "provider_name",     limit: 20,   null: false
    t.string  "loc",               limit: 2,    null: false
    t.string  "return_policy",     limit: 5000
    t.integer "provider_group_id"
  end

  add_index "ss_provider", ["provider_group_id"], name: "provider_group_id", using: :btree

  create_table "ss_provider_group", primary_key: "provider_group_id", force: true do |t|
    t.string "provider_group_name", limit: 20, null: false
  end

  create_table "ss_real_networks", id: false, force: true do |t|
    t.string "ind_identity_id1", limit: 14, null: false
    t.string "ind_identity_id2", limit: 14, null: false
  end

  add_index "ss_real_networks", ["ind_identity_id2"], name: "ind_identity_id2", using: :btree

  create_table "ss_repin", primary_key: "repin_id", force: true do |t|
    t.integer   "wardrobe_id",                null: false
    t.string    "item_id",         limit: 33, null: false
    t.integer   "src_wardrobe_id"
    t.timestamp "repin_time",                 null: false
  end

  add_index "ss_repin", ["item_id"], name: "item_id", using: :btree
  add_index "ss_repin", ["src_wardrobe_id"], name: "src_wardrobe_id", using: :btree
  add_index "ss_repin", ["wardrobe_id", "item_id", "src_wardrobe_id"], name: "repin", unique: true, using: :btree

  create_table "ss_report", id: false, force: true do |t|
    t.integer   "item_grp_id",                  null: false
    t.string    "item_id",         limit: 33,   null: false
    t.string    "ind_identity_id", limit: 14,   null: false
    t.string    "report_msg",      limit: 1024
    t.timestamp "report_time",                  null: false
  end

  add_index "ss_report", ["ind_identity_id"], name: "ind_identity_id", using: :btree

  create_table "ss_size_chart", id: false, force: true do |t|
    t.string "item_id",        limit: 33, null: false
    t.string "size_code_type", limit: 3,  null: false
    t.string "size_code",      limit: 6,  null: false
  end

  create_table "ss_style", primary_key: "user_id", force: true do |t|
    t.float "style_val", null: false
  end

  create_table "ss_style_rec_mandm", id: false, force: true do |t|
    t.string "stylist_id",     limit: 14, null: false
    t.string "item_id_1",      limit: 33, null: false
    t.string "item_id_2",      limit: 33, null: false
    t.float  "affinity_score",            null: false
  end

  add_index "ss_style_rec_mandm", ["item_id_1"], name: "item_id_1", using: :btree
  add_index "ss_style_rec_mandm", ["item_id_2"], name: "item_id_2", using: :btree

  create_table "ss_style_rec_similar", id: false, force: true do |t|
    t.string "stylist_id",     limit: 14, null: false
    t.string "item_id_1",      limit: 33, null: false
    t.string "item_id_2",      limit: 33, null: false
    t.float  "affinity_score",            null: false
  end

  add_index "ss_style_rec_similar", ["item_id_1"], name: "item_id_1", using: :btree
  add_index "ss_style_rec_similar", ["item_id_2"], name: "item_id_2", using: :btree

  create_table "ss_stylist", primary_key: "stylist_id", force: true do |t|
    t.string "stylist_screenname",  limit: 15,   null: false
    t.string "stylist_name",        limit: 50,   null: false
    t.string "stylist_description", limit: 1000
    t.float  "freshness",                        null: false
  end

  add_index "ss_stylist", ["stylist_screenname"], name: "stylist_screenname", unique: true, using: :btree

  create_table "ss_tags", primary_key: "tag_id", force: true do |t|
    t.string "tag_name",        limit: 20,  null: false
    t.string "tag_description", limit: 512
    t.string "tag_remark",      limit: 512
  end

  create_table "ss_temp_style_rec_similar_mandm", id: false, force: true do |t|
    t.string  "item_id_1", limit: 33, null: false
    t.string  "item_id_2", limit: 33, null: false
    t.integer "magic",     limit: 3,  null: false
  end

  add_index "ss_temp_style_rec_similar_mandm", ["item_id_2"], name: "item_id_2", using: :btree

  create_table "ss_temptbl_counts", primary_key: "repin_id", force: true do |t|
    t.integer "like_count",  null: false
    t.integer "msg_count",   null: false
    t.integer "repin_count", null: false
  end

  create_table "ss_wardrobe", primary_key: "wardrobe_id", force: true do |t|
    t.string    "ind_identity_id", limit: 14, null: false
    t.string    "wardrobe_name",   limit: 20, null: false
    t.timestamp "creation_time",              null: false
  end

  add_index "ss_wardrobe", ["ind_identity_id"], name: "ind_identity_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.string   "oauth_expires_at"
    t.text     "email"
    t.text     "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
