class UsersController < ApplicationController

helper_method :sort_column, :sort_direction
  
  def show
    @user = User.find(params[:id])
    @products = @user.products
    @products = @products.order(sort_column + " " + sort_direction)
    @products = @products.paginate(:page => params[:page], :per_page => 15)
  end
  
  private 
  
  def sort_column
    @products.column_names.include?(params[:sort]) ? params[:sort] : "item_name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  
end 
